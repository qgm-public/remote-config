#!/usr/bin/env bash

set -e

#trap 'handleError' ERR

handleError() {
    echo ""
    echo "Opps - das hätte nicht passieren dürfen!"
    echo "Es ist ein Fehler aufgetreten."
}

#-----------------------------------------------------
# Variables
#-----------------------------------------------------
COLOR_RED=`tput setaf 1`
COLOR_GREEN=`tput setaf 2`
COLOR_YELLOW=`tput setaf 3`
COLOR_BLUE=`tput setaf 4`
COLOR_RESET=`tput sgr0`
SILENT=false
ALLOW_ROOT=false

print() {
    if [[ "${SILENT}" == false ]] ; then
        echo -e "$@"
    fi
}

showLogo() {
    print "${COLOR_BLUE}"
    print " ,-----.    ,----.       ,--.,--------. "
    print "'  .-.  '  '  .-./       |  |'--.  .--' "
    print "|  | |  |  |  | .---.    |  |   |  |    "
    print "'  '-'  '-.'  '--'  |.--.|  |   |  |    "
    print " \`-----'--' \`------' '--'\`--'   \`--'    "
    print ""
    print "Installiert nötige Pakete, um "
    print "virtuelle Maschinen mit Virtualbox "
    print "betreiben zu können. "
    print "${COLOR_RESET}"
}


showLogo

print "Du wirst nun aufgefordert, dein Benutzerpasswort einzugeben,"
print "um Änderungen am System vornehmen zu können." 
print ""
print "Sollen die zum ausführen von virtuellen Maschinen nötigen "
print "Softwarepakete installiert werden? (j/n) "

read -s -n 1 ans 
print ""

if [ "x$ans" != "xj" ] && [ "x$ans" != "xJ" ]; then 
	print "${COLOR_RED}Auf Benutzerwunsch abgebrochen.${COLOR_RESET}"
	exit 1
fi

sudo ls > /dev/null 2>&1

echo "Paketquellen anpassen"
for l in google-chrome.list google-earth-pro.list vscodium.list oracle-virtualbox.list; do
	sudo rm -f /etc/apt/sources.list.d/$l > /dev/null 2>&1
done

echo "deb [arch=amd64 signed-by=/usr/share/keyrings/oracle-virtualbox-2016.gpg] https://download.virtualbox.org/virtualbox/debian bionic contrib" > /tmp/oracle-virtualbox.list 

sudo mv /tmp/oracle-virtualbox.list /etc/apt/sources.list.d/oracle-virtualbox.list
sudo chown root /etc/apt/sources.list.d/oracle-virtualbox.list 
wget -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | sudo gpg --dearmor --yes --output /usr/share/keyrings/oracle-virtualbox-2016.gpg > /dev/null 2>&1

sudo apt update

print "${COLOR_GREEN}[OK]${COLOR_RESET}" 

echo "Installiere Virtualbox... " 
sudo DEBIAN_FRONTEND=noninteractive apt -y install virtualbox-7.0 
print "${COLOR_GREEN}[OK]${COLOR_RESET}" 

echo "Führe Systemupdate durch... " 
sudo DEBIAN_FRONTEND=noninteractive apt -y full-upgrade 
sudo DEBIAN_FRONTEND=noninteractive apt -y autoremove
print "${COLOR_GREEN}[OK]${COLOR_RESET}" 


echo "Lösche Cache Partition und installiere grub nach /dev/sda "

ump=$(mount | grep virtualbox | awk '{print $1}')
if [ "x$ump" != "x" ]; then 
	sudo umount $ump
fi
ump=$(mount | grep cache | awk '{print $1}')
if [ "x$ump" != "x" ]; then 
	sudo umount $ump
fi

sudo mkdir -p /home/virtualbox
sudo sed -i "s/\/cache/\/home\/virtualbox/" /etc/fstab
sudo sed -i "s/ro,//" /etc/fstab
sudo grub-install /dev/sda
sudo mkfs.ext4 -F /dev/sda6
sudo mount -a
sudo chown -R laptop.users /home/virtualbox
ln -s /home/virtualbox /home/laptop/virtualbox






print "${COLOR_GREEN}[OK]${COLOR_RESET}" 


print "${COLOR_GREEN}[OK]${COLOR_RESET}" 

print ""
print "${COLOR_GREEN}Installation abgeschlossen.${COLOR_RESET}"



