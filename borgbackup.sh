#!/usr/bin/env bash

set -e

#trap 'handleError' ERR

handleError() {
    echo ""
    echo "Oops - das hätte nicht passieren dürfen!"
    echo "Es ist ein Fehler aufgetreten."
}

#-----------------------------------------------------
# Variables
#-----------------------------------------------------
COLOR_RED=`tput setaf 1`
COLOR_GREEN=`tput setaf 2`
COLOR_YELLOW=`tput setaf 3`
COLOR_BLUE=`tput setaf 4`
COLOR_RESET=`tput sgr0`
SILENT=false
ALLOW_ROOT=false

VORTA_TEMPLATE="https://codeberg.org/qgm-public/remote-config/raw/branch/master/files/vorta/vorta-init.template"

print() {
    if [[ "${SILENT}" == false ]] ; then
        echo -e "$@"
    fi
}

showLogo() {
    print "${COLOR_BLUE}"
    print " ,-----.    ,----.       ,--.,--------. "
    print "'  .-.  '  '  .-./       |  |'--.  .--' "
    print "|  | |  |  |  | .---.    |  |   |  |    "
    print "'  '-'  '-.'  '--'  |.--.|  |   |  |    "
    print " \`-----'--' \`------' '--'\`--'   \`--'    "
    print ""
    print "Installiert nötige Pakete, um "
    print "Backups auf die QG NAS erstellen"
    print "zu können."
    print "${COLOR_RESET}"
}


showLogo
print ""
print "Sollen die zur Erstellung von Backups nötigen "
print "Softwarepakete installiert werden? (j/n) "

read -s -n 1 ans 
print ""

if [ "x$ans" != "xj" ] && [ "x$ans" != "xJ" ]; then 
	print "${COLOR_RED}Auf Benutzerwunsch abgebrochen.${COLOR_RESET}"
	exit 1
fi

sudo ls > /dev/null 2>&1

echo 
echo "Vorta flatpak deinstallieren" 
echo 
flatpak --noninteractive --assumeyes install vorta
flatpak --noninteractive --assumeyes remove vorta
flatpak --noninteractive --assumeyes install vorta
flatpak --noninteractive --assumeyes update 
print "${COLOR_GREEN}[OK]${COLOR_RESET}" 

echo 
echo "Getting template..."
wget -q $VORTA_TEMPLATE -O - | sed "s/USERNAME/$USER/" > ~/.vorta-init.json
print "${COLOR_GREEN}[OK]${COLOR_RESET}" 


if [ ! -f ~/.ssh/id_qgmbackup ]; then 
	echo -n "Erzeuge SSH-Key... " 
	mkdir -p ~/.ssh > /dev/null 2>&1
	chown $USER ~/.ssh
	chmod 700 ~/.ssh
	ssh-keygen -q -t ed25519 -N '' -f ~/.ssh/id_qgmbackup <<<y >/dev/null 2>&1
	print "${COLOR_GREEN}[OK]${COLOR_RESET}"
fi

if [ -f ~/.ssh/id_qgmbackup ]; then
	BACKUP_USER=$USER
	BACKUP_PUBKEY=$(cat ~/.ssh/id_qgmbackup.pub)
else
	echo "Etwas ist schiefgegangen."
	exit 1
fi


print ""
print "${COLOR_GREEN}Installation abgeschlossen.${COLOR_RESET}"
print "${COLOR_RED}"
print ""
print "---------------------------------------------------------------"
print "Übermittle die folgenden Informationen an den Administrator,"
print "damit dein Zugang zum Backupserver eingerichtet werden kann."
print ""
print "Dazu einfach den gesamten Text kopieren und via Mail/Mattermost" 
print "übermitteln - es sind keine geheimen Informationen enthalten."
print "---------------------------------------------------------------"
print "${COLOR_RESET}"
echo "Benutzer:"
echo $BACKUP_USER

echo "Public Key:"
echo $BACKUP_PUBKEY
echo
