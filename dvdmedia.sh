#!/usr/bin/env bash

set -e

trap 'handleError' ERR

handleError() {
    echo ""
    echo "Opps - das hätte nicht passieren dürfen!"
    echo "Es ist ein Fehler aufgetreten."
}

#-----------------------------------------------------
# Variables
#-----------------------------------------------------
COLOR_RED=`tput setaf 1`
COLOR_GREEN=`tput setaf 2`
COLOR_YELLOW=`tput setaf 3`
COLOR_BLUE=`tput setaf 4`
COLOR_RESET=`tput sgr0`
SILENT=false
ALLOW_ROOT=false

print() {
    if [[ "${SILENT}" == false ]] ; then
        echo -e "$@"
    fi
}

showLogo() {
    print "${COLOR_BLUE}"
    print " ,-----.    ,----.       ,--.,--------. "
    print "'  .-.  '  '  .-./       |  |'--.  .--' "
    print "|  | |  |  |  | .---.    |  |   |  |    "
    print "'  '-'  '-.'  '--'  |.--.|  |   |  |    "
    print " \`-----'--' \`------' '--'\`--'   \`--'    "
    print ""
    print "Installiert nötige Pakete, um "
    print "DVDs kopieren zu können."
    print "${COLOR_RESET}"
}


showLogo

print "Sie werden nun aufgefordert, ihr Benutzerpasswort einzugeben,"
print "um Änderungen am System vornehmen zu können." 
print ""
print "Sollen die zum abspielen und kopieren von DVDs nötigen "
print "Softwarepakete installiert werden? (j/n) "

read -s -n 1 ans 
print ""

if [ "x$ans" != "xj" ] && [ "x$ans" != "xJ" ]; then 
	print "${COLOR_RED}Auf Benutzerwunsch abgebrochen.${COLOR_RESET}"
	exit 1
fi

sudo ls > /dev/null 2>&1

echo 
echo -n "Paketdatenbank aktualisieren... " 
sudo apt update > /dev/null 2>&1
print "${COLOR_GREEN}[OK]${COLOR_RESET}" 

echo -n "Installiere libdvdcss2... " 
sudo DEBIAN_FRONTEND=noninteractive apt -y install libdvdcss2 > /dev/null 2>&1
print "${COLOR_GREEN}[OK]${COLOR_RESET}" 

echo -n "Konfiguriere libdvdcss2... " 
sudo DEBIAN_FRONTEND=noninteractive dpkg-reconfigure -p critical libdvd-pkg  > /dev/null 2>&1
print "${COLOR_GREEN}[OK]${COLOR_RESET}" 

echo -n "Installiere handbrake... "
sudo DEBIAN_FRONTEND=noninteractive apt -y install handbrake > /dev/null 2>&1
print "${COLOR_GREEN}[OK]${COLOR_RESET}"

print ""
print "${COLOR_GREEN}Installation abgeschlossen.${COLOR_RESET}"


